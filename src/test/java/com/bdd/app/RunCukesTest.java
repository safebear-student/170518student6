package com.bdd.app;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by CCA_Student on 17/05/2018.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        tags = "~@to-do",

        /** Multiple tags would be: tags = {"~@to-do", "@high-risk"},
         *
        */

        glue = "com.bdd.app",
        features = {
                "classpath:simplerisk.features/user_management.feature",
                "classpath:simplerisk.features/accounts.feature"}
/** can just have the name of the feature directory if you want to run all tests in there
 *  features = "classpath:simplerisk.features"
 */
)
public class RunCukesTest {
}
