Feature: User management

  User Story:
  In order to manage users
  As an administrator
  I need CRUD permissions to user accounts

  Rules:
  - Non-admin users have no permissions over other users
  - There are 2 types of non-admin accounts
  -- Risk managers
  -- Asset managers
  - Non admin users must only be able to see risks and assets in their own group
  -Admin must be able to reset users passwords

  Questions:
  - Do admin users need access to all accounts or only the accounts in their group?

  To Do:
  - Force reset of password on account creation

  Domain language
  Group = users are defined by which group they belong in
  CRUD = Create, Read, Update, Delete
  Admin permissions = access to CRUD  on risks, users, assets for all groups
  risk_management = Only CRUD risks
  asset_management = Only CRUD assets


  Background:
    Given a user called simon with administration permissions and password S@feB3ar
    And a user called tom with risk_management permissions and password S@feB3ar


    @high-impact
    Scenario Outline: The admin checks a user's details
      When simon is logged in with password S@feB3ar
      Then he is able to view <user>'s account
      Examples:
        | user |
        |tom   |


    @high-risk
    @to-do
    Scenario Outline:  A user's password is reset by the admin
      Given a <user> has lost his password
      When simon is logged in with password <password>
      Then simon can reset <user>'s password
      Examples:
        | user |
        |tom   |


